# [Golang][Web] `range` in http/template

A tiny demo app for `range` in [http/template](https://golang.org/pkg/html/template/).

## Usage

Use [Git](https://git-scm.com/) to clone this repo:

```
$ git clone https://gitlab.com/cwchen/GolangWebTemplateRange.git
```

Run this program:

```
$ cd GolangWebTemplateRange
$ go run main.go
```

Visit http://localhost:8080 for the result:

![Using range in http/template](images/golang-web-template-range.PNG)

## Copyright

2018, Michael Chen; Apache 2.0.
